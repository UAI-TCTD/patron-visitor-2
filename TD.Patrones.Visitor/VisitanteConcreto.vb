﻿Imports TD.Patrones.Visitor

Public Class VisitanteConcreto
    Implements IVisitor

    Public Sub Visitar(v As DiscoRigido) Implements IVisitor.Visitar
        Console.WriteLine(String.Format("Disco Rígido s/n {0}", v.Serial))
    End Sub

    Public Sub Visitar(v As Procesador) Implements IVisitor.Visitar
        Console.WriteLine(String.Format("Procesador s/n {0}", v.Serial))
    End Sub

    Public Sub Visitar(v As PlacaBase) Implements IVisitor.Visitar
        Console.WriteLine(String.Format("Placa Base s/n {0}", v.Serial))
    End Sub
End Class
