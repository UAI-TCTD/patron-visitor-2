﻿Module Module1

    Sub Main()
        Dim v As IVisitor
        v = New VisitanteConcreto

        Dim dr As New DiscoRigido("1231DSFFSD3-DR")
        Dim pb As New PlacaBase("dkk3nndj12313-PB")
        Dim p As New Procesador("9393SKKK4K3-P")

        p.Aceptar(v)
        pb.Aceptar(v)
        dr.Aceptar(v)

        Console.ReadKey()
    End Sub

End Module
