﻿Imports TD.Patrones.Visitor

Public Class PlacaBase
    Inherits Componente
    Public Sub New(s As String)
        MyBase.New(s)
    End Sub
    Public Overrides Sub Aceptar(v As IVisitor)
        v.Visitar(Me)
    End Sub
End Class
